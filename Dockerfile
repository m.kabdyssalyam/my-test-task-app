FROM openjdk:8u181-jre-alpine

COPY /build/libs/TestProblem-1.0-SNAPSHOT.jar ./TestProblem-1.0-SNAPSHOT.jar

ENTRYPOINT exec java -jar TestProblem-1.0-SNAPSHOT.jar