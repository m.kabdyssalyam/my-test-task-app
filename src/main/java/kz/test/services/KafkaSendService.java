package kz.test.services;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KafkaSendService {

    private final static Logger logger = Logger.getLogger(KafkaSendService.class.getName());

    private Producer<Long, Long> producer;

    public KafkaSendService(Producer<Long, Long> producer) {
        this.producer = producer;
    }

    public void send() {

        Long currentTimeStamp = System.currentTimeMillis();

        ProducerRecord<Long, Long> record = new ProducerRecord<Long, Long>("timestamps", currentTimeStamp, currentTimeStamp);

        try {
            producer.send(record).get();
        } catch (InterruptedException | ExecutionException e) {
            logger.log(Level.SEVERE, "Error sending to kafka");
        }

        logger.info("Sent to kafka at time " + currentTimeStamp);
    }
}
