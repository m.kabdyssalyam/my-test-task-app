package kz.test.services;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import kz.test.repositories.TimestampRepository;
import kz.test.state.ApplicationState;
import kz.test.state.Status;
import kz.test.utils.ConnectionUtil;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.record.Record;

import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KafkaReadService {

    private final static Logger logger = Logger.getLogger(KafkaReadService.class.getName());

    private Consumer<Long, Long> consumer;

    private TimestampRepository timestampRepository;

    public KafkaReadService(Consumer<Long, Long> consumer, TimestampRepository timestampRepository) {
        this.consumer = consumer;
        this.timestampRepository = timestampRepository;
    }

    public void start() {
        logger.info("Started consumer");

        while (true) {
            try {
                if (ApplicationState.status == Status.OK) {
                    ConsumerRecords<Long, Long> records = consumer.poll(Duration.ofMillis(100));

                    if (records.iterator().hasNext()) {
                        logger.info("Has records");
                        ConsumerRecord<Long, Long> record = records.iterator().next();
                        timestampRepository.insertTimestamp(record.value());
                        consumer.commitAsync();
                    }
                } else {
                    Thread.sleep(5000);
                }
            } catch (InterruptedException e) {
                logger.log(Level.SEVERE, "Internal error");
            } catch (CommunicationsException e) {
                ApplicationState.status = Status.FAIL;
                ConnectionUtil.closeConnection();
                logger.log(Level.WARNING, "Failed connecting to database");
            }
        }
    }
}
