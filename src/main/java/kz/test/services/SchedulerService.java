package kz.test.services;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

public class SchedulerService {

    private static final Logger logger = Logger.getLogger(SchedulerService.class.getName());

    /**
     *Single threaded Timer to guarantee that task will execute in chronological order
     */
    private TimerTask task;

    public SchedulerService(TimerTask task) {
        this.task = task;
    }

    public void scheduleAndStart(long period) {
        logger.info("Scheduling...");

        Timer timer = new Timer();

        timer.schedule(task, 0, period);

        logger.info("scheduled");
    }

}
