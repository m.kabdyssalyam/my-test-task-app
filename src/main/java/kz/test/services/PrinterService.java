package kz.test.services;

import kz.test.repositories.TimestampRepository;

import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class PrinterService {

    private static final Logger logger = Logger.getLogger(PrinterService.class.getName());

    private TimestampRepository timestampRepository;

    public PrinterService(TimestampRepository timestampRepository) {
        this.timestampRepository = timestampRepository;
    }

    public void printAllTimestamps() {
        List<Long> timestamps = timestampRepository.getAllTimestamps();

        timestamps.forEach((timestamp) -> {
            logger.info(String.valueOf(timestamp));
        });
    }
}
