package kz.test;

import kz.test.repositories.TimestampRepository;
import kz.test.services.KafkaReadService;
import kz.test.services.KafkaSendService;
import kz.test.services.PrinterService;
import kz.test.services.SchedulerService;
import kz.test.tasks.ReconnectTask;
import kz.test.tasks.SpamDbWithTimeTask;
import kz.test.utils.KafkaUtils;
import kz.test.utils.Migrator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.TimerTask;
import java.util.logging.Logger;

public class Runner {

    private static final String PRINT_MODE = "p";

    private static final long ONE_SECOND = 1000L;

    private static final Logger logger = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) throws FileNotFoundException {
        logger.info("Application started");

        new Migrator().run();

        TimestampRepository timestampRepository = new TimestampRepository();

        if (args.length != 0 && PRINT_MODE.equals(args[0])) {
            logger.info("Running in print mode");
            runPrintMode(timestampRepository);
        } else {
            logger.info("Running in write mode");
            runWriteMode(timestampRepository);
        }
        logger.info("Application exited");
    }

    private static void runPrintMode(TimestampRepository timestampRepository) {
        new PrinterService(timestampRepository).printAllTimestamps();
    }

    private static void runWriteMode(TimestampRepository timestampRepository) {
        TimerTask spamDbTask = new SpamDbWithTimeTask(new KafkaSendService(KafkaUtils.createProducer()));
        TimerTask reconnectTask = new ReconnectTask();

        new SchedulerService(spamDbTask).scheduleAndStart(ONE_SECOND);

        new SchedulerService(reconnectTask).scheduleAndStart(5 * ONE_SECOND);

        new KafkaReadService(KafkaUtils.createConsumer(), timestampRepository).start();
    }
}
