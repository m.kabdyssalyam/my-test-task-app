package kz.test.utils;

import java.util.ArrayList;
import java.util.List;

public class Topics {
    public static final String TIMESTAMPS = "timestamps";

    public static List<String> getTopics() {
        List<String> topics = new ArrayList<String>();
        topics.add(TIMESTAMPS);

        return topics;
    }
}
