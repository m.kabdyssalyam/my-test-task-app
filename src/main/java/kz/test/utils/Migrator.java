package kz.test.utils;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Not actual migrator
 * Run once to create single table if not exists
 */
public class Migrator {

    private static final Logger logger = Logger.getLogger(Migrator.class.getName());

    public void run() {
        try {
            Scanner scanner = new Scanner(Migrator.class.getResourceAsStream("/sql/init.sql"));

            String script = scanner.nextLine();
            Connection connection = ConnectionUtil.getConnection();

            connection.prepareStatement(script).executeUpdate();
        } catch (CommunicationsException e) {
            logger.log(Level.SEVERE, "easd");
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error connection");
        }

        logger.info("Migration successful!");
    }
}
