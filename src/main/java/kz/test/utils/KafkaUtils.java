package kz.test.utils;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KafkaUtils {

    private final static Logger logger = Logger.getLogger(KafkaUtils.class.getName());

    public static Producer<Long, Long> createProducer() {
        Properties props = null;

        try {
            props = new Properties();
            props.load(KafkaUtils.class.getResourceAsStream("/kafka-producer.properties"));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error occurred on loading kafka");
        }

        return new KafkaProducer<Long, Long>(props);
    }

    public static Consumer<Long, Long> createConsumer() {
        Properties props = null;

        try {
            props = new Properties();
            props.load(KafkaUtils.class.getResourceAsStream("/kafka-consumer.properties"));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error occurred on loading kafka");
        }

        Consumer<Long, Long> consumer = new KafkaConsumer<Long, Long>(props);
        consumer.subscribe(Topics.getTopics());

        return consumer;
    }
}
