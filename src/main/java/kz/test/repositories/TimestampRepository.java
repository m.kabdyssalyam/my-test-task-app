package kz.test.repositories;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import kz.test.utils.ConnectionUtil;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TimestampRepository {

    private static final Logger logger = Logger.getLogger(TimestampRepository.class.getName());

    public void insertTimestamp(Long timestamp) throws CommunicationsException {
        try {
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO timestamps(record_time) VALUES(?)");

            statement.setTimestamp(1, Timestamp.from(Instant.ofEpochMilli(timestamp)));

            Integer result = statement.executeUpdate();

            logger.info(result.toString());
        } catch (CommunicationsException e) {
            throw e;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error inserting timestamp", e);
        }
    }

    public List<Long> getAllTimestamps() {
        try {
            List<Long> result = new ArrayList<Long>();

            Connection connection = ConnectionUtil.getConnection();
            ResultSet resultSet = connection.prepareStatement("SELECT record_time FROM timestamps").executeQuery();

            while (resultSet.next()) {
                Timestamp timestamp = resultSet.getTimestamp(1);
                result.add(timestamp.getTime());
            }

            return result;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error selecting from timestamp");
        }

        return null;
    }
}
