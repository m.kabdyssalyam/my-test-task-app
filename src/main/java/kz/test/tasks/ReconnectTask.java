package kz.test.tasks;

import kz.test.state.ApplicationState;
import kz.test.state.Status;
import kz.test.utils.ConnectionUtil;

import java.sql.Connection;
import java.util.TimerTask;

public class ReconnectTask extends TimerTask {

    @Override
    public void run() {
        if (ApplicationState.status == Status.FAIL) {
            Connection connection = ConnectionUtil.getConnection();

            if (connection != null) {
                ApplicationState.status = Status.OK;
            }
        }
    }
}

