package kz.test.tasks;

import kz.test.services.KafkaSendService;

import java.util.TimerTask;
import java.util.logging.Logger;

public class SpamDbWithTimeTask extends TimerTask {

    private static final Logger logger = Logger.getLogger(SpamDbWithTimeTask.class.getName());

    private KafkaSendService kafkaSendService;

    public SpamDbWithTimeTask(KafkaSendService kafkaSendService) {
        this.kafkaSendService = kafkaSendService;
    }

    @Override
    public void run() {
        kafkaSendService.send();
    }
}
