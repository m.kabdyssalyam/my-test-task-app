import com.github.dockerjava.api.command.CreateContainerCmd;
import kz.test.Runner;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import util.ConnectionUtil;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DatabaseConnectionFailureTest {

    private static final MySQLContainer container = new MySQLContainer();

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DatabaseConnectionFailureTest.class.getName());

    /*@Test
    public void shouldBeOkAndInRange() throws SQLException {
        Connection connection = ConnectionUtil.getConnection();

        ResultSet rs = connection.prepareStatement("SELECT record_time FROM timestamps").executeQuery();
        long finish = System.currentTimeMillis();

        while (rs.next()) {
            Assert.assertTrue(rs.getTimestamp(1).getTime() < finish);
        }
    }*/

    @Test
    public void shouldFail() throws FileNotFoundException {
        container.start();
        assertTrue(container.isRunning());
    }
}
